<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Adding Barryvdh\HandleCors package to api middleware to enable CORS.
Route::middleware(['api', \Barryvdh\Cors\HandleCors::class])->namespace('API')->group(function () {
    Route::post('dates', 'DateController@getDate');
});
