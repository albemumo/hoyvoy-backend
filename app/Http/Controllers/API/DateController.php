<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\StoreDatePost;
use App\Services\DateService;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;


class DateController extends Controller
{

    /**
     * Show date isLeap and day translated from post date.
     *
     * @param StoreDatePost $request
     * @param DateService $dateService
     * @return \Illuminate\Http\Response
     */
    public function getDate(StoreDatePost $request)
    {
        $dateService = App::make(DateService::class); // Instance of DateService

        return response()->json(
            [
                'data' => [
                    'leapYear' => $dateService->checkIsLeapYear($request->date),
                    'day' => $dateService->getDayLocalesFromDate($request->date, ['es', 'ca']),
                ]
            ]
        );
    }
}
