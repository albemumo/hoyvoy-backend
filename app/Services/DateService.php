<?php
namespace App\Services;

use Jenssegers\Date\Date;

class DateService implements DateServiceInterface
{
    /**
     * Check date leap year
     *
     * @param $date
     * @return bool
     */
    public function checkIsLeapYear($date) {
        return Date::createFromFormat('Y-m-d', $date)->isLeapYear();
    }

    /**
     * Get day of date translated
     *
     * @param $date
     * @param $locales
     * @param string $splitter
     * @return string
     */
    public function getDayLocalesFromDate($date, $locales, $splitter = '/') {
        $day = '';
        $i = 1;
        foreach ($locales as $locale) {
            Date::setLocale($locale);
            $day .= Date::createFromFormat('Y-m-d', $date)->format('l');
            count($locales) - 1 == $i ? $day .= $splitter : null;
            $i++;
        }
        return $day;
    }
}