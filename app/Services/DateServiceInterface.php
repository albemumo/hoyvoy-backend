<?php
/**
 * Created by PhpStorm.
 * User: albemumo
 * Date: 11/02/19
 * Time: 20:01
 */

namespace App\Services;


interface DateServiceInterface
{
    /**
     * @param $date
     * @return mixed
     */
    public function checkIsLeapYear($date);

    /**
     * @param $date
     * @param $locales
     * @param string $splitter
     * @return mixed
     */
    public function getDayLocalesFromDate($date, $locales, $splitter = '/');
}