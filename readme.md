Requirements
------------
- Composer
- PHP >= 7.1.3
- OpenSSL PHP Extension
- PDO PHP Extension
- Mbstring PHP Extension
- Tokenizer PHP Extension
- XML PHP Extension
- Ctype PHP Extension
- JSON PHP Extension
- BCMath PHP Extension

Installation
------------
- Open terminal
- Clone the repository `git clone https://albemumo@bitbucket.org/albemumo/hoyvoy-backend.git`
- Go to the project root. `cd hoyvoy-backend`
- Execute `composer install` command.
- Copy .env.example to .env `cp .env.example .env`
- Run `php artisan key:generate`

Running local development server
--------------------------------
- Inside project root folder, execute server: `php artisan serve`
- Access url http://127.0.0.1:8000/

Running Tests
-------------
To run tests execute the next command inside project root folder:
- `./vendor/bin/phpunit`


External libraries installed 
----------------------------

- jenssegers/date https://github.com/jenssegers/date
This library extends Carbon multi-language support.

- barryvdh/laravel-cors https://github.com/barryvdh/laravel-cors
This library allows you to send Cross-Origin Resource Sharing headers with Laravel middleware configuration, needed to test in local.

Documentation
-------------

This project provides a documentation for the code and API. You can access it after project is installed.

`NOTE: To run documentation local development server running is necessary.`

[Sami](http://127.0.0.1:8000/docs/sami/index.html)








