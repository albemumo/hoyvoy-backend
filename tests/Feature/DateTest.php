<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DateTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    /**
     * Test Api Post Date invalid.
     *
     * @return void
     */
    public function testApiPostDateInvalid()
    {
        $response = $this->json('POST', '/api/dates');
        $response
            ->assertStatus(422);

        $response->assertJson([
            'message' => 'The given data was invalid.',
            'errors'  => [
                'date' => [
                    'The date field is required.',
                ],
            ],
        ]);
    }

    /**
     * Test Api Post Date valid.
     *
     * @return void
     */
    public function testApiPostDateValid()
    {
        $response = $this->json('POST', '/api/dates', [
            'date' => '1996-07-05'
        ]);
        $response
            ->assertStatus(200);

        $response->assertJson([
            'data'  => [
                'leapYear' => true,
                'day' => 'viernes/divendres',
            ],
        ]);
    }
}
