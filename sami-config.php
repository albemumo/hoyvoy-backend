<?php

use Sami\Sami;
use Symfony\Component\Finder\Finder;

$iterator = Finder::create()
    ->files()
    ->name('*.php')
    ->exclude('node_modules')
    ->exclude('resources')
    ->exclude('database')
    ->exclude('config')
    ->exclude('routes')
    ->exclude('bootstrap')
    ->exclude('storage')
    ->exclude('vendor')
    ->in(__DIR__ .'/app');
return new Sami($iterator, [
    'title'                => 'HoyVoy Backend',
    'build_dir'            => __DIR__.'/build/%version%',
    'cache_dir'            => __DIR__.'/cache/%version%',
    'default_opened_level' => 2,
]);